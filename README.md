# forkio-step-project

Technologies list:
- package manager npm
- toolkit gulp
- sass
- javascript

The scripts in src folder show and hide the drop-down menu and a little bit helping with adaptive

To create actual build version run "gulp build"
To update the current build while development process run "gulp dev"

Team:
- Taras Zaslavskiy
- Oleksandr Lytovchenko

Tasks:
- Taras Zaslavskiy:
1. Section Revolutionaty Editor
2. Section Here is what you get
3. Section Fork Subscribing Pricing
- Oleksandr Lytovchenko
1. Header Section
2. Section People Are Talking About Fork
3. gulp config
