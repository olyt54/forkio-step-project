const gulp = require("gulp"),
    sass = require("gulp-sass"),
    autoprefixer = require("gulp-autoprefixer"),
    concat = require("gulp-concat"),
    clean = require("gulp-clean"),
    imagemin = require("gulp-imagemin"),
    cleanCSS = require("gulp-clean-css"),
    browserSync = require("browser-sync").create(),
    minifyjs = require("gulp-js-minify"),
    uglify = require("gulp-uglify");

const paths = {
    src: {
        html: "public/index.html",
        styles: "src/scss/**/*.scss",
        js: "src/js/*.js",
        img: "src/img/**/*[.jpeg.jpg.png.svg]"
    },
    dist: {
        styles: "public/dist/css",
        js: "public/dist/js",
        img: "public/dist/img",
        dist: "public/dist"
    }
};

const cleanDist = () => (
    gulp.src(paths.dist.dist, {allowEmpty: true})
        .pipe(clean())
);

const buildCSS = () => (
    gulp.src(paths.src.styles)
        .pipe(sass().on('error', sass.logError))
        .pipe(concat("styles.min.css"))
        .pipe(autoprefixer({
            cascade: false
        }))
        .pipe(cleanCSS({compatibility: 'ie8'}))
        .pipe(gulp.dest(paths.dist.styles))
);

const buildJS = () => (
    gulp.src(paths.src.js)
        .pipe(concat("scripts.min.js"))
        .pipe(minifyjs())
        .pipe(uglify())
        .pipe(gulp.dest(paths.dist.js))
);

const minImg = () => (
    gulp.src(paths.src.img)
        .pipe(imagemin())
        .pipe(gulp.dest(paths.dist.img))
);

const watcher = () => {
    browserSync.init({
        server: {
            baseDir: "./public"
        }
    });
    gulp.watch(paths.src.html).on("change", browserSync.reload);
    gulp.watch(paths.src.js, buildJS).on("change", browserSync.reload);
    gulp.watch(paths.src.styles, buildCSS).on("change", browserSync.reload);

};

gulp.task("css", buildCSS);
gulp.task("js", buildJS);
gulp.task("clean", cleanDist);
gulp.task("imgmin", minImg);

gulp.task("build", gulp.series(
    cleanDist,
    buildCSS,
    buildJS,
    minImg
));
gulp.task("dev", watcher);






