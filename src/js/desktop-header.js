function moveMenu() {
    const menu = document.getElementsByClassName("header__navigation__menu")[0],
        navigation = document.getElementsByClassName("header__navigation")[0],
        wrapper = document.getElementsByClassName("header__navigation__menu-wrapper")[0];

    if (window.screen.availWidth > 992 || window.screen.width > 992) {
        navigation.removeChild(menu);
        wrapper.prepend(menu);
    }

    window.addEventListener("resize", () => {
        if ((window.screen.availWidth > 992 || window.screen.width > 992) && [...navigation.children].find((item) => {
            if (item === menu) {
                return item;
            }
        })) {
            navigation.removeChild(menu);
            wrapper.prepend(menu);
        } else if ((window.screen.availWidth <= 992 || window.screen.width <= 992) && [...wrapper.children].find((item) => {
            if (item === menu) {
                return item;
            }
        })) {
            wrapper.removeChild(menu);
            navigation.append(menu);
        }
    })
}

moveMenu();