function toggleMenu() {
    const btn = document.getElementsByClassName("header__navigation__burger-menu")[0],
        navList = document.getElementsByClassName("header__navigation__menu")[0];

    btn.addEventListener("click", () => {
        navList.classList.toggle("header__navigation__menu--active");
        btn.classList.toggle("header__navigation__burger-menu--active");
    });
}

toggleMenu();